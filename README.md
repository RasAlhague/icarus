# Icarus - Fallen pixel

## Story

Icarus is a rather jumpy pixel in the sky. One day icarus accidentally fell from the sky and now wants to get back into his place.

## Genre

 - 2D Platformer

## Gameplaymechanics

 - Jumping
 - Walljumping
 - Dash
 - Ceiling walking
 - Wallbouncing
 - Collect coins
 
## Gameobjects:

 - Player
 - Flowers
 - Boxes
 - Jumppads
 - Ugrades
 - NPC pixels
 - Spikes
 - traps
 
## Levels:

### Level 1:

 - Easy ascent
 - no complex jumps
 - player talks to itself about it being stupid

#### Mechanics:
 
 - Simple jumping
 - Wall Jumps
 - Jump pads