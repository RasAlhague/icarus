# Icarus Fallen pixel

## Story/Lore

### Player character

#### Name: 
Icarus

#### Description: 
A pixel which is sometimes to active and cocky and fell of his place to the ground because of that.

### Enemies

#### Name:
Pixel Reaper

#### Description:
Pixel which also like icarus lost their place but instead of keeping their memories clumped together and become a monstrosity.
They have a lot of forms but share one similarity. They assimilate every free roaming pixels they can find on contact.

## Mechanics:

### Walljump
- jump of the wall to continue climbing
- possibly add a limit to how many times until a cooldown happens

### Dashing 
- dash toward the position of the cursor on the screen 
- draw a trajectory or jump direction marker
- give it a cooldown, which could possibly be upgraded
- deactivate air control when dashing
- unaffected by gravity for a short time
- only reaches a certain distance with exception of zero gravity

### Jumppads
- pads which boost your jump
- boost 180° in the oposite direction
- different strenghts

### GravityChanging rooms:
- rooms with interval based gravity changes
- normal abilities allowed
- goes from low to high to negative gravity (ceiling is gravity point)

### Rotating rooms
- rooms which rotate constantly
- interval based

### Puzzles
- time puzzels to open a door
- way puzzles
- only really simple puzzles to give a bit of a change

### destructable walls
- certain walls get destroyed when the player dashes into them
- breaks down into pixels

### Zero gravity rooms
- no jumping in normal sense
- jumppads
- no gravity
- bouncy walls 
- only dashes

### Basic enemies
- called pixel reaper
- simple enemies which the player must avoid
- later with an upgrade the player can dash into enemies to destroy them
- creates knockback
- enemies are kind of evil pixel clusters

### Gravity depending color changes
- Changes the color sceme depending on high, low, normal, negative or zero gravity

## Raw level design (mechanics):

### Pre Levels:
- Display of icarus talking with other pixels in the sky
- Icarus leaves his place
- Icarus plays around, other pixels warn him to not play around too much or he will falling
- Icarus dont listen to them and gets cocky and ultimativly falls
- Display of icarus falling out of his place for a couple of seconds
- Display of icarus crashing onto the ground
- Icarus getting desperate and thinks about his situation
- Icarus now starts going back up
- Transition to level 1

### Level 1: Basics

#### Stage 1:
- Introduces walljumps, the spawnpoint, respawning

#### Stage 2: 
- Introduces spikes
- More complex jumps

#### Stage 3:
- Hardest jumps yet
- Adding of enemies
- Reaching goal for next level

### Level 2: Dashing

#### Stage 1:
- Introduces dash mechanic
- Focused on Dashing

#### Stage 2: 
- Now Jumps with Dashes will be nessasary to complete the level
- Easy stuff to learn to combine the mechanics

#### Stage 3:
- Now complex jumps will be nessasary
- Enemies will be placed now too
- wallbreakthrough dashes will be introduced
- Reaching goal for next level

### Level 3: Jumppads and puzzles

#### Stage 1:
- Introduces Jumppads
- Teaches how to combine jumppads with old mechanics

#### Stage 2: 
- Jumppad mania level
- Focused on speeding through the level
- increase combination difficulty

#### Stage 3:
- Adds puzzles to jumppads
- You have to jump on jumppads in a certain order to reach the goal and open doors
- Enimes with jumppads on the body will be added
- time trials
- Reaching of next level

#### Optional Area:
- Enemy dashing can be found 

### Level 4: Gravity changes

#### Stage 1:
- Low gravity and high gravity Rooms
- all old mechanics in use

#### Stage 2: 
- Gravity changing rooms
- Normal rooms rarely

#### Stage 3: 
- Manual changing of gravity for simple gravity based puzzles
- Advanced gravity changing room challenges and low and high gravity rooms

#### Stage 4:
- Combination of all
- Reaching of next level

### Level 5: No gravity

#### Stage 1: 
- Completly without gravity
- No jumps possible only dashes

#### Stage 2:
- Adding jumppads to it
- Some normal rooms now
- Harder rooms

#### Stage 3:
- now a mix of all rooms and mechanics
- Hard Levels
- Quickchanges between no gravity and gravity

#### Stage 4:
- Reaching next level

### Level 6: Nearing the end

#### Stage 1:
- Reaching the endgame Area
- use of all mechanics
- Reaching of final stage

#### Stage Final:
- Something like a small bossfight where you have to use a couple of mechanics to beat a big pixel reaper.
- No direct attacking instead fighting through the environment.

### After: 
- Icarus gets shown with his pixel friends reunited. Icarus gets back into his place

## Leveldesign (Story)

### Pre Levels:
- Display of icarus talking with other pixels in the sky
- Icarus leaves his place
- Icarus plays around, other pixels warn him to not play around too much or he will falling
- Icarus dont listen to them and gets cocky and ultimativly falls
- Display of icarus falling out of his place for a couple of seconds
- Display of icarus crashing onto the ground
- Icarus getting desperate and thinks about his situation
- Icarus now starts going back up
- Transition to level 1

### Level 1: Basics
- Icarus wakes up
- when player first walks over spawnpoint:
    - Icarus wonders what this strange effects were
- When player first time respawns:
    - Icarus talks about understanding what this was and that he only yet heared about it
- When icarus first comes to a wall to high to jump over:
    

#### Stage 1:

#### Stage 2: 

#### Stage 3:

### Level 2: Dashing

#### Stage 1:

#### Stage 2: 

#### Stage 3:

### Level 3: Jumppads and puzzles

#### Stage 1:

#### Stage 2: 

#### Stage 3:

#### Optional Area:

### Level 4: Gravity changes

#### Stage 1:

#### Stage 2: 

#### Stage 3: 

#### Stage 4:

### Level 5: No gravity

#### Stage 1: 

#### Stage 2:

#### Stage 3:

#### Stage 4:

### Level 6: Nearing the end

#### Stage 1:

#### Stage Final:

### After: 
