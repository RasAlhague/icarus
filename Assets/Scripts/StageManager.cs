﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts
{
    public static class StageManager
    {
        public static int CurrentStage { get; set; }
        public static int TotalStages { get; set; }
        public static int LevelNumber { get; set; }
        public static float RunTime { get; set; }
        public static int Coins { get; set; }
        public static int Respawns { get; set; }
        public static int NextStage { get; set; }
    }
}
