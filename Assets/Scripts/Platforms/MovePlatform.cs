﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovePlatform : MonoBehaviour
{
    [SerializeField]
    private float _moveSpeed;
    private int _nextPoint = 1;
    private bool _backwards;

    public List<Transform> destinations = new List<Transform>();

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if(transform.position == destinations[_nextPoint].position)
        {
            _nextPoint++;

            if(_nextPoint >= destinations.Count)
            {
                destinations.Reverse();
                _nextPoint = 1;
            }
        }

        float step = _moveSpeed * Time.deltaTime;

        transform.position = Vector2.MoveTowards(transform.position, destinations[_nextPoint].position, step);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.CompareTag("Player"))
        {
            collision.collider.transform.SetParent(transform);
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.collider.CompareTag("Player"))
        {
            collision.collider.transform.SetParent(null);
        }
    }
}
