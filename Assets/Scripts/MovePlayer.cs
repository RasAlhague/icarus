﻿using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovePlayer : MonoBehaviour
{
    public CharacterController2D controller;
    public float runSpeed = 40f;

    private float horizontalMove = 0f;
    private bool jump = false;
    [SerializeField]
    private float afterImageWaitTime = 0.2f;
    private float currentAfterImageTime;
    private Rigidbody2D _rb;
    [SerializeField]
    private float _maxVelocity = -10;

    private void Start()
    {
        _rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        horizontalMove = Input.GetAxisRaw("Horizontal") * runSpeed;

        if (Input.GetButtonDown("Jump"))
        {
            jump = true;

            var zeroVerticalVelocity = _rb.velocity;

            zeroVerticalVelocity.y = 0;
            _rb.velocity = zeroVerticalVelocity;
        }

        var velocity = _rb.velocity;

        if (velocity.y < _maxVelocity)
        {
            velocity.y = _maxVelocity;
            _rb.velocity = velocity;
        }

        //if (velocity.y > Mathf.Abs(_maxVelocity))
        //{
        //    velocity.y = Mathf.Abs(_maxVelocity);
        //    _rb.velocity = velocity;
        //}
    }

    private void FixedUpdate()
    {
        controller.Move(horizontalMove * Time.fixedDeltaTime, false, jump);
        jump = false;

        if (!controller.m_Grounded)
        {
            if (currentAfterImageTime <= 0)
            {
                PlayerAfterImagePool.Instance.GetFromPool();
                currentAfterImageTime = afterImageWaitTime;
            }
            else
            {
                currentAfterImageTime -= Time.deltaTime;
            }

        }
    }
}
