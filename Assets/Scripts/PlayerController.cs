﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private Rigidbody2D rb;
    
    public int coins;
    public float health = 1;
    public Transform spawnPoint;

    private void Update()
    {
        if(Input.GetButtonDown("Respawn"))
        {
            Respawn();
        }

        if (health <= 0)
        {
            Respawn();
        }
    }

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    public void Respawn()
    {
        var sr = GetComponent<SpriteRenderer>();
        sr.enabled = false;

        rb.velocity = new Vector2(0, 0);
        rb.position = spawnPoint.position;
        sr.enabled = true;
        health = 1;

        Debug.Log("Respawn");
    }

    public void SetSpawn(Transform newSpawn)
    {
        spawnPoint = newSpawn;
        Debug.Log("Set Spawn");
    }

    public void TakeDamage(float amount)
    {
        health -= amount;
    }

    public void AddCollectable(string type)
    {
        switch(type)
        {
            case "coin":
                coins++;
            break;
            default:
                throw new ArgumentException("The collectable does not exist!");
        }
    }
}
