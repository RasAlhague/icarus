﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Goal : MonoBehaviour
{
    public Sprite reachedGoalSprite;
    public GameObject player;
    public GameManager gameManager;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.CompareTag("Player"))
        {
            player.GetComponent<MovePlayer>().enabled = false;
            player.GetComponent<Rigidbody2D>().velocity = new Vector2(0,0);
            var spriteRenderer = GetComponent<SpriteRenderer>();
            spriteRenderer.sprite = reachedGoalSprite;

            gameManager.LoadNextStage();
        }
    }
}
