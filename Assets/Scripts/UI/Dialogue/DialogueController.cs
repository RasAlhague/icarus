﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogueController : MonoBehaviour
{
    [SerializeField]
    private TMPro.TMP_Text speakerTextObject;
    [SerializeField]
    private TMPro.TMP_Text textboxTextObject;
    [SerializeField]
    private Image textBoxImage;

    public string speakerText;
    public string textboxText;

    // Update is called once per frame
    void Update()
    {
        if(speakerTextObject.text == speakerText && textboxTextObject.text == textboxText)
        {
            return;
        }

        speakerTextObject.text = speakerText;
        textboxTextObject.text = textboxText;
    }

    private void ChangeState(bool active)
    {
        speakerTextObject.gameObject.SetActive(active);
        textboxTextObject.gameObject.SetActive(active);
        textBoxImage.gameObject.SetActive(active);
    }

    public void ShowDialogue()
    {
        ChangeState(true);
    }

    public void HideDialogue()
    {
        ChangeState(false);
    }
}
