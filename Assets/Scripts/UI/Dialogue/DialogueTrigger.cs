﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueTrigger : MonoBehaviour
{
    private bool triggered = false;

    public DialogueController dialogueController;
    public string speaker;
    public string text;
    public float displayTime;

    private void Update()
    {
        if (triggered)
        {
            if(displayTime < 0)
            {
                dialogueController.HideDialogue();
            }

            displayTime -= Time.deltaTime;
        } 
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.CompareTag("Player"))
        {
            dialogueController.speakerText = speaker;
            dialogueController.textboxText = text;
            dialogueController.ShowDialogue();
            triggered = true;

        }
    }
}
