﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterPoolContainer : MonoBehaviour
{
    public readonly List<Sprite> chars = new List<Sprite>();
    public readonly List<Sprite> numbers = new List<Sprite>();
}
