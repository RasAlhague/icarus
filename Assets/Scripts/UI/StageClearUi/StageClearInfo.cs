﻿using Assets.Scripts;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StageClearInfo : MonoBehaviour
{
    public List<Sprite> numSprites = new List<Sprite>();
    public Image levelImage;
    public Image currentStageImage;
    public Image totalStagesImage;

    private void Start()
    {
        levelImage.sprite = numSprites[StageManager.LevelNumber];
        currentStageImage.sprite = numSprites[StageManager.CurrentStage];
        totalStagesImage.sprite = numSprites[StageManager.TotalStages];
    }
}
