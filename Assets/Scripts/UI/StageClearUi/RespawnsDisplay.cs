﻿using Assets.Scripts;
using Assets.Scripts.UI.StageClearUi;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RespawnsDisplay : MonoBehaviour
{
    public List<Sprite> numSprites = new List<Sprite>();
    public Image singleDigitImg;
    public Image tensDigitImg;

    // Start is called before the first frame update
    void Start()
    {
        singleDigitImg.sprite = numSprites[DigitHelper.GetPlace(StageManager.Respawns, Digits.Single)];
        tensDigitImg.sprite = numSprites[DigitHelper.GetPlace(StageManager.Respawns, Digits.Tens)];
    }
}
