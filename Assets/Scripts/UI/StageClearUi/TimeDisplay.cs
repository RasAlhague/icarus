﻿using Assets.Scripts;
using Assets.Scripts.UI.StageClearUi;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimeDisplay : MonoBehaviour
{
    public List<Sprite> numSprites = new List<Sprite>();
    public Image singleMinutesImg;
    public Image tensMinutesImg;
    public Image singleSecondsImg;
    public Image tensSecondsImg;

    // Start is called before the first frame update
    void Start()
    {
        var timeSpan = TimeSpan.FromSeconds(StageManager.RunTime);

        singleSecondsImg.sprite = numSprites[DigitHelper.GetPlace((int)timeSpan.TotalSeconds, Digits.Single)];
        tensSecondsImg.sprite = numSprites[DigitHelper.GetPlace((int)timeSpan.TotalSeconds, Digits.Tens)];
        singleMinutesImg.sprite = numSprites[DigitHelper.GetPlace((int)timeSpan.TotalMinutes, Digits.Single)];
        tensMinutesImg.sprite = numSprites[DigitHelper.GetPlace((int)timeSpan.TotalMinutes, Digits.Tens)];
    }
}
