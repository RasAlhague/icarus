﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.UI.StageClearUi
{
    enum Digits
    {
        Single = 1,
        Tens = 10,
        Hundreds = 100,
        Thousands = 1000,
        Tenthousands = 10000
    }

    static class DigitHelper
    {
        public static int GetPlace(int value, Digits place)
        {
            return ((value % ((int)place * 10)) - (value % (int)place)) / (int)place;
        }
    }
}
