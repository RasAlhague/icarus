﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpawnSetInfo : MonoBehaviour
{
    [SerializeField]
    private float fadeDuration = 3f;
    private bool spawnHasBeenSet = false;
    private float fadeTime;

    public Image spawnSetImage;

    private void Start()
    {
        fadeTime = fadeDuration;
    }

    private void Update()
    {
        if (spawnHasBeenSet)
        {
            spawnSetImage.color = new Color(1,1,1, spawnSetImage.color.a - 1f * Time.deltaTime);
        }

        if(fadeTime <= 0)
        {
            spawnHasBeenSet = false;
        }
    }

    public void SetSpawn()
    {
        spawnSetImage.color = new Color(1, 1, 1, 1);
        spawnHasBeenSet = true;
    }
}
