﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IngameMenuController : MonoBehaviour
{
    public GameObject player;
    public GameObject ingameMenuUi;
    public GameObject optionsMenuUi;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            ingameMenuUi.SetActive(!ingameMenuUi.activeInHierarchy);
            optionsMenuUi.SetActive(false);     
        }

        if (ingameMenuUi.activeInHierarchy || optionsMenuUi.activeInHierarchy)
        {
            player.GetComponent<MovePlayer>().enabled = false;
        }
        else
        {
            player.GetComponent<MovePlayer>().enabled = true;
        }
    }
}
