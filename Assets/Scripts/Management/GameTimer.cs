﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameTimer : MonoBehaviour
{
    public float totalTime = 0;

    // Update is called once per frame
    void Update()
    {
        totalTime += Time.deltaTime;
    }
}
