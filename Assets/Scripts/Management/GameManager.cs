﻿using Assets.Scripts;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    [SerializeField]
    private int currentStage;
    [SerializeField]
    private int totalStages;
    [SerializeField]
    private int level;
    [SerializeField]
    private int nextStage;

    private void Start()
    {
        StageManager.TotalStages = totalStages;
        StageManager.CurrentStage = currentStage;
        StageManager.LevelNumber = level;
    }

    public void LoadNextStage()
    {
        var playerObject = GameObject.FindGameObjectWithTag("Player");

        StageManager.NextStage = nextStage;
        StageManager.RunTime = GetComponent<GameTimer>().totalTime;
        StageManager.Coins = playerObject.GetComponent<PlayerController>().coins;

        SceneManager.LoadScene(SceneManager.sceneCountInBuildSettings - 1);
    }

    public void LoadMainMenu()
    {
        SceneManager.LoadScene(0);
    }

    public void ReloadCurrentLevel()
    {
        StageManager.RunTime = 0;
        StageManager.Coins = 0;

        StageManager.TotalStages = totalStages;
        StageManager.CurrentStage = currentStage;
        StageManager.LevelNumber = level;

        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}
